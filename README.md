## Steps to install Docker Compose

yum install -y python-pip

pip install docker-compose

docker-compose up -d --force-recreate --build (launches containers in detached mode & re-builds everything)

docker-compose up  --force-recreate --build (launches containers & opens on the same prompt but re-builds everything)